var randomstring = require("randomstring"),
	moment = require('moment');

module.exports = function(app, apiPath) {
	var readersPath = 'readers';

	/* service: /readers */
	app.util.handler({
		verb: 'get',
		endpoint: apiPath + readersPath,
		callback: function(req, res){
			var userId = (req.userId)? req.userId: null;

			new Promise(function(success, failed){
				var sql ="SELECT u.uuid, u.firstName, u.lastName, u.email "
					+"FROM users u JOIN authors a ON u.id = a.user_id WHERE u.id != ?";	
				
				app.ddbb.query(sql, [userId], function(err, rows, fields){
					if (err) {
						failed(err);
					}
					success(rows);
				});

			}).then(
				function(rows){
					var _rows = [];
					rows.forEach(function(row){
						var _row = {
							uuid: row.uuid,
							firstName: row.firstName,
							lastName: row.lastName,
							email: row.email,
						};
						_rows.push(_row);
					});
					res.status(200);
					res.json(_rows);			
				},
				function(err){
					console.log(err);
					res.status(500);
					res.send("Error server..");			
				}
			);
		}
	});

}


