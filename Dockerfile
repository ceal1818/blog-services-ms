FROM mysql:5.7

ENV MYSQL_ROOT_PASSWORD 1234
ENV MYSQL_DATABASE blog_ddbb
ENV MYSQL_USER blog-user
ENV MYSQL_PASSWORD 1234

VOLUME [ "/var/lib/mysql" ]

COPY backup/blog-ddbb.sql /docker-entrypoint-initdb.d